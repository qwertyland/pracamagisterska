#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;
auto INF = numeric_limits<int>::max();

struct flowgraph_t {
	int n, s, t;
	vector<vector<int>> e, f, c, idx;
	vector<bool> vis;
	flowgraph_t(){}
	flowgraph_t(int _n, int _s, int _t){
		n = _n;
		s = _s;
		t = _t;
		e.resize(n);
		f.resize(n);
		c.resize(n);
		idx.resize(n);
		vis.resize(n);
	}
	void addEdge(int a, int b, int c1, int c2){
		e[a].push_back(b);
		e[b].push_back(a);
		c[a].push_back(c1);
		c[b].push_back(c2);
		f[a].push_back(0);
		f[b].push_back(0);
		idx[a].push_back(e[b].size() - 1);
		idx[b].push_back(e[a].size() - 1);
	}
	void clearFlow(){
		for (int i = 0; i < n; i++){
			f[i].clear();
		}
	}
};

using FlowGraph = flowgraph_t;

int augmentingPath(FlowGraph& g, int v, int f){
	//cout << "homhom " << v << "\n";
	if (v == g.t){
		return f;
	}
	g.vis[v] = true;
	for (int i = 0; i < g.e[v].size(); i++){
		//cout << "hh " << g.e[v][i] << "\n";
		int w = g.e[v][i];
		int af = g.c[v][i] - g.f[v][i];
		if ((!g.vis[w]) && (af > 0)){
			int mf = augmentingPath(g, w, min(f, af));
			if (mf > 0){
				g.f[v][i] += mf;
				g.f[w][g.idx[v][i]] -= mf;
				return mf;
			}
		}
	}
	return 0;
}

void FordFulkerson(FlowGraph& g){
	for (int i = 0; i < g.n; i++){
		g.vis[i] = 0;
	}
	int f;
	do {
		f = augmentingPath(g, g.s, INF);
		g.vis[g.s] = false;
		if (f == 0){
			for (int i = 0; i < g.n; i++){
				g.vis[i] = 0;
			}
			f = augmentingPath(g, g.s, INF);
			//cout << "boooooooomba " << f << "\n";
		}
	} while (f > 0);
}


long long checkMaxMatchings(int n){
	long long maskMax = 1;
	for (int i = 0; i < n * n; i++){
		maskMax *= 2;
	}
	long long count = 0;
	for (long long mask = 0; mask < maskMax; mask++){
		//cout << "a1\n";
		FlowGraph g(2 * n + 2, 0, 2 * n + 1);
		for (int k = 0; k < n; k++){
			for (int l = 0; l < n; l++){
				//cout << "b\n";
				if (mask & (1LL << (n *k + l))){
					g.addEdge(1 + k, 1 + n + l, 1, 0);
				}
			}
		}
		//cout << "c\n";
		for (int i = 0; i < n; i++){
			g.addEdge(0, i + 1, 1, 0);
			//cout << "d\n";
			g.addEdge(1 + n + i, 2 * n + 1, 1, 0);
		}
		//cout << "a\n";
		FordFulkerson(g);
		//cout << "b " << g.f[0][0] << "\n";
		bool ok = true;
		for (int i = 0; i < n; i++){
			if (g.f[0][i] == 0){
				ok = false;
				break;
			}
		}
		if (ok){
			count++;
		}
	}
	return count;
}

int main(){
	int n;
	cin >> n;
	long long cnt, nn = 1;
	for (int i = 0; i < n * n; i++){
		nn *= 2;
	}
	cnt = checkMaxMatchings(n);
	cout << n << " " << cnt << " " << nn << "\n";
}



